import Handlebars from 'handlebars';
import { readFileSync, writeFileSync } from 'node:fs'

Handlebars.registerHelper('inc', function (value, options) {
  return parseInt(value) + 1;
});
Handlebars.registerHelper('eq', (a, b) => a == b)

const template = readFileSync('./template.hbs').toString('utf8')
const html = Handlebars.compile(template);
export default function createHtml(raw, name) {
  const groups = {};
  
  for (const [group, groupSchedule] of Object.entries(raw.data)) {
    const hours = []
    for (const [dayOfWeek, dayScedule] of Object.entries(groupSchedule)) {
      for (const [endHour, status] of Object.entries(dayScedule)) {
        const hour = parseInt(endHour) - 1
        if (!hours[hour]) {
          hours[hour] = {title: `${hour}-${endHour}`, schedule: []}
        }
        hours[hour].schedule[parseInt(dayOfWeek)] = status
      }
    }
    groups[group] = html({
      weekdays: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      hours
    })
  }
  return groups
}
